#!/bin/sh
#
#    Copyright 2022 Ibai Roman
#
#    This file is part of Operating System Updater.
#
#    Operating System Updater is free software: you can redistribute it and/or
#    modify it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Operating System Updater is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Operating System Updater. If not, see
#    <http://www.gnu.org/licenses/>.

[ "$_DEBUG" = "on" ] && set -x

error() {
    printf "\033[1;31merror: %s\033[0m\n" "$1" >&2
}

is_installed() {
    COMMAND=$1

    [ -x "$(command -v "$COMMAND")" ]
}

as_root() {
    if [ "$(id -u)" -eq 0 ]; then
        "$@"
    elif is_installed "sudo" ; then
        sudo "$@"
    elif is_installed "doas" ; then
        doas "$@"
    elif is_installed "run0" ; then
        run0 "$@"
    else
        error "No permissions to execute '$*'"
    fi
}

BACKUP_DIR="$HOME/.osu"

if is_installed "apt" ; then
    as_root apt update
    as_root apt full-upgrade
    as_root apt autoremove --purge
    as_root apt autoclean
    as_root apt purge "?config-files"
    apt list '?installed ?name(linux-image)'
    uname -r

    if [ -d "$BACKUP_DIR" ] ; then
        apt-mark showauto > "$BACKUP_DIR/apt.auto.list"
        apt-mark showmanual > "$BACKUP_DIR/apt.manual.list"
        apt-mark showhold > "$BACKUP_DIR/apt.hold.list"
    fi
fi

if is_installed "rpm-ostree" ; then
    rpm-ostree upgrade

    if [ -d "$BACKUP_DIR" ] ; then
        rpm-ostree status --json | \
            jq -c '.deployments[] | select (.booted) | .["requested-packages"][]' | \
            sed 's/"//g' > "$BACKUP_DIR/rpm-ostree.packages.list"
        rpm-ostree status --json | \
            jq -c '.deployments[] | select (.booted) | .["requested-base-removals"][]' | \
            sed 's/"//g' > "$BACKUP_DIR/rpm-ostree.base_removals.list"
        rpm-ostree status --json | \
            jq -c '.deployments[] | select (.booted) | .["requested-local-packages"][]' | \
            sed 's/"//g' > "$BACKUP_DIR/rpm-ostree.local_packages.list"
    fi
elif is_installed "dnf" ; then
    as_root dnf upgrade --refresh
    as_root dnf autoremove
    as_root dnf clean all
    dnf list --installed kernel*
    uname -r

    if [ -d "$BACKUP_DIR" ]; then
        dnf list --installed | awk '{print $1}' > "$BACKUP_DIR/dnf.installed.list"
    fi
fi

if is_installed "apk" ; then
    as_root apk update
    as_root apk upgrade
    as_root apk cache clean
    apk info | grep linux-
    uname -r

    if [ -d "$BACKUP_DIR" ]; then
        apk info -q > "$BACKUP_DIR/apk.installed.list"
    fi
fi

if is_installed "freebsd-update" ; then
    as_root freebsd-update fetch install
    as_root pkg update
    as_root pkg upgrade

    if [ -d "$BACKUP_DIR" ] ; then
        pkg prime-list > "$BACKUP_DIR/pkg.prime.list"
    fi
fi

if is_installed "pacman" ; then
    as_root pacman --sync --refresh
    as_root pacman --sync --sysupgrade
    pacman --query | grep linux
    uname -r

    if [ -d "$BACKUP_DIR" ]; then
        pacman --query --explicit --quiet > "$BACKUP_DIR/pacman.explicit.list"
        pacman --query --deps --quiet > "$BACKUP_DIR/pacman.deps.list"
    fi
fi

if is_installed "winget" ; then
    as_root winget upgrade --all

    if [ -d "$BACKUP_DIR" ] ; then
        winget export -o "$BACKUP_DIR/winget.json" --disable-interactivity
    fi
fi

if is_installed "snap" ; then
    as_root snap refresh

    if [ -d "$BACKUP_DIR" ] ; then
        snap list | awk 'NR>1 {print $1","$4","$6}' > "$BACKUP_DIR/snap.list"
    fi
fi

if is_installed "flatpak" ; then
    flatpak update
    flatpak uninstall --unused

    if [ -d "$BACKUP_DIR" ] ; then
        flatpak list --app | awk -F'\t' '{print $2","$4","$5}' > \
            "$BACKUP_DIR/flatpak.app.list"
        flatpak list --runtime | awk -F'\t' '{print $2","$4","$5}' > \
            "$BACKUP_DIR/flatpak.runtime.list"
    fi
fi
